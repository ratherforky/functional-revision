import Prelude hiding (foldr, sum, product, length)

foldr :: (a -> b -> b) -> b -> [a] -> b
foldr trans startVal []     = startVal
foldr trans startVal (x:xs) = trans x acc
  where
    acc = foldr trans startVal xs

sum :: [Int] -> Int
sum = foldr (+) 0

product :: [Int] -> Int
product = foldr (*) 1

length :: [a] -> Int
length = foldr (\x acc -> acc + 1) 0

foldl :: (b -> a -> b) -> b -> [a] -> b
foldl trans startVal xs = go startVal xs
  where
  --go :: b -> [a] -> b
    go acc []     = acc
    go acc (x:xs) = go (trans acc x) xs

-- foldl and foldr different when binary function is not commutative
divsR :: [Int] -> Int
divsR = foldr (\x acc -> x `div` acc) 1

divsL :: [Int] -> Int
divsL = foldr (\acc x -> x `div` acc) 1
