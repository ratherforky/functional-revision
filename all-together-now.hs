wordWrapper :: Int -> String -> String
wordWrapper charsPerLine
  = unlines                 -- String
  . map unwords             -- [String]
  . takeWhileUnderCharLimit -- [[String]]
  . words                   -- [String]
  where
    takeWhileUnderCharLimit :: [String] -> [[String]]
    takeWhileUnderCharLimit = snd . foldr f (0, [])

    f :: String -> (Int, [[String]]) -> (Int, [[String]])
    f word (numChars, accLines)
      | numChars' <= charsPerLine = (numChars', consHead word accLines)
      | otherwise = (length word, [word] : accLines)
      where
        numChars' = length word + 1 + numChars

    consHead :: a -> [[a]] -> [[a]]
    consHead x []       = [[x]]
    consHead x (ys:yss) = (x:ys):yss

longString = "The less you eat, drink and read books; the less you go to the theatre, the dance hall, the public house; the less you think, love, theorize, sing, paint, fence, etc., the more you save-the greater becomes your treasure which neither moths nor dust will devour-your capital. The less you are, the more you have; the less you express your own life, the greater is your alienated life-the greater is the store of your estranged being."

prop_nolongerthanN n str 
  = n > 0 && (all ((<= n) . length) $ words str)
  ==> all (<= n)
    . map length
    . lines 
    $ wordWrapper n str
