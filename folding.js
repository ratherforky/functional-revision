function sum(xs) {
  let acc = 0;
  console.log('acc: ' + acc);
	for (let x in xs) {
    acc = x + acc;
    console.log('acc: ' + acc);
  }
  console.log('acc: ' + acc);
	return acc;
}

function length(xs) {
	let acc = 0;
	for (let x in xs) {
    acc = 1 + acc;
  }
	return acc;
}

function foldl(f, k, xs) {
	let acc = k;
	for (let x in xs) {
    acc = f(x, acc);
  }
	return acc;
}

function add(x, y) {
  return x + y;
}

function sumFold(xs){
	return foldl(add, 0, xs);
}

function lengthFold(xs){
	return foldl((x, acc) => acc + 1, 0, xs);
}

console.log(sum([1,2,3,4,5]));
console.log(sumFold([1,2,3,4,5]));
console.log(length([1,2,3,4,5]));
console.log(lengthFold([1,2,3,4,5]));