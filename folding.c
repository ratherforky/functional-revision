#include <stdio.h>

#define LEN 3

int foldr(int (*trans)(int, int), int startVal, int* xs) {
  int acc = startVal;
  for (int i = LEN - 1; i >= 0; i--) {
    acc = trans(xs[i], acc);
  }
  return acc;
}

int add(int x, int y) {
  return x + y;
}

int sumFold(int* xs){
	return foldr(add, 0, xs);
}

int main() {
  int xs[LEN] = {1,2,3};
  printf("Sum of [1,2,3]: %d\n", sumFold(xs));
  return 0;
}