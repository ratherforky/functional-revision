import Data.List (sort)
import Test.QuickCheck

-- input -> x2 -> +10 -> x3 -> -7 -> output
functionMachine :: Int -> Int
functionMachine = (\x -> x - 7)
                . (* 3)
                . (+ 10) 
                . (* 2)

sortWords :: String -> String
sortWords = unwords
          . sort 
          . words

sortWordsOnLine :: String -> String
sortWordsOnLine
  = unlines
  . sort
  . map sortWords
  . lines

test = "The United States is also a one-party state but,\n\
       \with typical American extravagance,\n\
       \they have two of them."
