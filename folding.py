def sum(xs):
	acc = 0
	for x in xs:
		acc = acc + x
	return acc

def length(xs):
	acc = 0
	for x in xs:
		acc = acc + 1
	return acc

def foldl(f, k, xs):
	acc = k
	for x in xs:
		acc = f(acc, x)
	return acc

def add(x, y):
  return x + y

def sumFold(xs):
	return foldl(add, 0, xs)

def lengthFold(xs):
	return foldl(lambda acc, x: acc + 1, 0, xs)

print(sum([1,2,3,4,5]))
print(sumFold([1,2,3,4,5]))
